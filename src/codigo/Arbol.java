/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import javax.swing.JOptionPane;

/**
 *
 * @author esteb
 */
public class Arbol {

    Nodo raiz = null;

    public void ingresar(Sitios dato) {
        ingresar(dato, raiz);
    }

    private void ingresar(Sitios dato, Nodo aux) {
        if (raiz == null) {
            raiz = new Nodo(dato);
        } else {
            double direccion = Math.random();
            if (direccion > 0.5) {
                if (aux.getIzq() == null) {
                    if (altura(raiz.getIzq()) - altura(raiz.getDer()) > 2 || altura(raiz.getIzq()) - altura(raiz.getDer()) < -2) {
                        ingresar(dato, aux);
                    } else {
                        aux.setIzq(new Nodo(dato));
                    }
                } else {
                    ingresar(dato, aux.getIzq());
                }
            } else {
                if (aux.getDer() == null) {
                    if (altura(raiz.getIzq()) - altura(raiz.getDer()) > 2 || altura(raiz.getIzq()) - altura(raiz.getDer()) < -2) {
                        ingresar(dato, aux);
                    } else {
                        aux.setDer(new Nodo(dato));
                    }
                } else {
                    ingresar(dato, aux.getDer());
                }
            }

        }
    }

    public void blancear(Nodo aux) {

    }

    public int altura(Nodo aux) {
        if (aux == null) {
            return -1;
        } else {
            return 1 + Math.max(altura(aux.getDer()), altura(aux.getIzq()));
        }
    }
}
